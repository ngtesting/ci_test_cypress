describe('Baidu Test', function () {
    it('Search', function () {
        cy.visit("https://www.baidu.com")

        cy.get("#kw", {timeout: 2000}).type("禅道")

        cy.wait(100)

        cy.get("#su", { timeout: 2000 }).click()

        cy.title().should("contain", "禅道")
    })
})
